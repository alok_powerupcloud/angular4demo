import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

import { CanActivateAuthGuard } from '../guards/can-activate-auth.guard';

const routes: Routes = [
  {
  path: 'home',
  component: HomeComponent,
  canActivate: [CanActivateAuthGuard],
  }
];  

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeComponent],
  exports:[
    RouterModule
  ]
})
export class HomeModule { }
