import { Injectable } from '@angular/core';

@Injectable()
export class CustomersService {

  customers = [
    {
      name: 'Alok',
      status: 'Active'
    },
    {
      name: 'Manikandan',
      status: 'Active'
    },
    {
      name: 'Jhon',
      status: 'Inactive'
    }
  ];

  constructor() { }

  getCustomers() {
    return this.customers;
  }

  addCustomer(name: string, status: string) {
    this.customers.push({ name: name, status: status });
  }

  updateStatus(id: number, status: string) {
    this.customers[id].status = status
  }

  deleteCustomer(index: number) {
    this.customers.splice(index, 1);
  }

  getCustomer(index: number) {
    return this.customers[index];
  }
}
