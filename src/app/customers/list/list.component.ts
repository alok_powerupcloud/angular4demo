import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomersService } from '../customers.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  // @Input() parentTitle : string;
  // @Output() callByChild = new EventEmitter();

  customers = [];

  constructor(private customerService: CustomersService) { }

  ngOnInit() {
    this.customers = this.customerService.getCustomers();
    // this.callByChild.emit('pass val from child to parent @Output');
  }

}
