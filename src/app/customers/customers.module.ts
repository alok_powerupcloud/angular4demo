import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { RouterModule, Routes } from '@angular/router';
import { CustomersComponent } from './customers.component';
import { CustomersService } from './customers.service';
import { DataTablesModule } from 'angular-datatables';

const routes: Routes = [
  {
    path: 'customers',
    component: CustomersComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'detail/:id',
        component: DetailComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    CustomersComponent,
    ListComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    CustomersService
  ],
  exports: [
    RouterModule
  ]

})

export class CustomersModule { }
