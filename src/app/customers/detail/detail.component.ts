import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { CustomersService } from '../customers.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  // childTitle: string = 'Child Title';
  id: number;
  customer: {name: string, status: string};
  

  constructor(
    private customerService: CustomersService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params : Params) => {
        this.id = +params['id'];
        this.customer = this.customerService.getCustomer(this.id);
      }
    );
  }

}
