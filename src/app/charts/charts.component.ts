import { Component, OnInit, Input, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  @ViewChild('pieChartContainer') chartContainer: any;
  @ViewChild('lineChartContainer') chartContainer1: any;
  @ViewChild('barChartContainer') chartContainer2: any;
  @ViewChild('logChartContainer') chartContainer3: any;

  constructor() { }

  ngOnInit() {
    this.drawPiechart();
    this.drawLineChart();
    this.drawBarChart();
    this.drawLogChart();
  }

  /**
   * Method to draw pie chart
   */
  drawPiechart() {
    let chartConfig = {
      chart: {
          plotBackgroundColor: null as any,
          plotBorderWidth: null as any,
          plotShadow: false,
          type: 'pie',
          
      },
      title: {
          text: 'Highchart Pie chart Example'
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  style: {
                      //color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              },
              showInLegend: true
          }
      },
      series: [{
          name: 'Application Status',
          colorByPoint: true,
          data: [{
              name: 'Approved',
              y: 56.33,
              color:"#008000"
          }, {
              name: 'Pending',
              y: 24.03,
              color: "#ffff00"
              // sliced: true,
              // selected: true
          }, {
              name: 'Rejected',
              y: 10.38,
              color: "#FF0000"
          }]
      }]
  };
  Highcharts.chart(this.chartContainer.nativeElement, chartConfig);
  }

  /**
   * Method to create line chart
   */
  drawLineChart() {
    let chartConfig = {
      chart: {
          type: 'line',
      },

      title: {
          text: 'Highchart Line chart example'
      },
      // subtitle: {
      //     text: 'Source: WorldClimate.com'
      // },
      xAxis: {
        plotLines: [{
          color: 'green',
          dashStyle: 'longdashdot', // Style of the plot line. Default to solid
          value: 4,
          width: 2   
        }],
          categories: ['3 Months', '6 Months', '9 Months', '12 Months', 'YTD', 'Since Inception']         
      },
      yAxis: {
          title: {
              text: 'Performance'
          }
      },
      plotOptions: {  
          line: {
              dataLabels: {
                  enabled: true
              },
              enableMouseTracking: false,
              // shadow:{
              //   color: 'rgb(149,206,255)',
              //   // offsetX: 20,
              //   offsetY: 20,
              //   opacity: 0.5
              // }
          }
      },
      series: [{
          name: 'Cumulative return',
          data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5],
          // shadow: true,
      }]
  };
  
  Highcharts.chart(this.chartContainer1.nativeElement, chartConfig);
  }

  /**
   * Method to draw bar charts
   */
  drawBarChart() {
    let chartConfig = {
      chart: {
          type: 'column',
      },

      title: {
          text: 'Highchart Bar/Column chart example'
      },
      // subtitle: {
      //     text: 'Source: WorldClimate.com'
      // },
      xAxis: {
          categories: ['YTD', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
          title: {
              text: 'Monthly Performance'
          }
      },
      plotOptions: {
        column: {
            dataLabels: {
                enabled: true
            }
        }
      },
      credits: {
        enabled: false
      },
      series: [{
            name: '2016',
            data: [5, 3, 4, 7, 2,1,5,3,2,-1,-3,5]
        }, {
            name: '2017',
            data: [2, -2, -3, 2, 1]
        }]
  };
    Highcharts.chart(this.chartContainer2.nativeElement, chartConfig);
  }

  /**
   * Method to draw log chart
   */
  drawLogChart() {
    let chartConfig = {
      chart: {
          type: 'line',
      },

      title: {
          text: 'Highchart Logs chart example'
      },
      xAxis: {
        tickInterval: 1
      },
      yAxis: {
        type: 'logarithmic',
        minorTickInterval: 0.1
      },
      tooltip: {
        headerFormat: '<b>{series.name}</b><br />',
        pointFormat: 'x = {point.x}, y = {point.y}'
      },
      series: [{
        data: [1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
        pointStart: 1
    },
    {
        data: [10, 5,4,3,2,1],
        pointStart: 1
    }]
  };
    Highcharts.chart(this.chartContainer3.nativeElement, chartConfig);
  }
  
}
