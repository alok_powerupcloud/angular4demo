import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { CommonModule } from '@angular/common';
import { ChartsComponent } from './charts.component';

const routes: Routes = [
  {
  path: 'charts',
  component: ChartsComponent,
  } 
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChartsComponent],
  exports: [
    RouterModule
  ]
})
export class ChartsModule { }
