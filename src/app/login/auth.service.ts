import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {

  constructor() { }

  public getToken(): string {
    return (localStorage.getItem('accessToken')) ? JSON.parse(localStorage.getItem('accessToken')) : '';
  }

  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // whether or not the token is expired
    return true;
    // return tokenNotExpired(null, token);
  }

  isLoggedIn(): boolean {
    return (this.getToken()) ? true : false;
  }

  getSession(): any {
    return (localStorage.user) ? JSON.parse(localStorage.user) : '';
  }

  login(postBody: any) {
    if(postBody.email == 'alok.kamboj@yopmail.com' && postBody.password == 'alok') {
      return {
        status: 'success',
        message: 'You have successfully logged in',
        token: this.makeToken(),
      }
    } else {
      return {
        status: 'failed',
        message: 'Login credentials doesn\'t match'
      }
    }
    
  }

  logout() {
    return localStorage.removeItem('accessToken');
  }

  makeToken() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }
}
