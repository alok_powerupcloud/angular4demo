import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  response:any;
  loginForm: FormGroup;

  constructor(private authService: AuthService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    if(localStorage.getItem('accessToken')) {
      this.router.navigate(['home']);
    }
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
  }

  /**
   * Method to login the user
   */
  login() {
    this.response = this.authService.login(this.loginForm.value);
    if(this.response.status == 'success' && this.response.token){
      localStorage.accessToken = JSON.stringify(this.response.token);
      this.toastr.success(this.response.message);
      this.router.navigate(['home']);
    } else {
      this.toastr.error(this.response.message);
    }
  }

}
