import { Injectable } from '@angular/core';

@Injectable()
export class EmployeeService {

  employees = [
    {
      id: 1,
      first_name: 'Alok',
      last_name: 'Kamboj'
    },
    {
      id: 1,
      first_name: 'Manikandan',
      last_name: 'Sivanathan'
    },
    {
      id: 1,
      first_name: 'Jack',
      last_name: 'Bower'
    },
    {
      id: 1,
      first_name: 'Patrick',
      last_name: 'Dupont'
    },
    {
      id: 1,
      first_name: 'Barack',
      last_name: 'Obama'
    },
    {
      id: 1,
      first_name: 'Max',
      last_name: 'Well'
    },
    {
      id: 1,
      first_name: 'Jack',
      last_name: 'Sparrow'
    },
    {
      id: 1,
      first_name: 'Warden',
      last_name: 'Well'
    },
    {
      id: 1,
      first_name: 'Sachin',
      last_name: 'Ten'
    },
    {
      id: 1,
      first_name: 'Anil',
      last_name: 'Mohan'
    },
    {
      id: 1,
      first_name: 'Rose',
      last_name: 'Well'
    },
    {
      id: 1,
      first_name: 'Hero',
      last_name: 'Hero'
    },
    {
      id: 1,
      first_name: 'François',
      last_name: 'Holland'
    },
    {
      id: 1,
      first_name: 'Michel',
      last_name: 'Popo'
    },
    {
      id: 1,
      first_name: 'Chuck',
      last_name: 'Norris'
    },

  ];

  constructor() { }

  getEmployees() {
    return this.employees;
  }
}
