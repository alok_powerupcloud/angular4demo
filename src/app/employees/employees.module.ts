import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {EmployeesComponent} from './employees.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { DataTablesModule } from 'angular-datatables';
import { EmployeeService } from './employee.service';

const routes: Routes = [
  {
  path: 'employees',
  component: EmployeesComponent,
  children: [
    {
      path: '',
      component: ListComponent
    },
    {
      path: 'detail/:id',
      component: DetailComponent
    }
  ]
  } 
];

@NgModule({
  declarations: [
    EmployeesComponent,
    ListComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(routes)
  ],
  providers:[
    EmployeeService
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeesModule {}
