import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', redirectTo: '/employees', pathMatch: 'full'},
  // {path: '', redirectTo: '/login', pathMatch: 'full'},
  // {path: 'pagenotfound', component: PageNotFoundComponent},
  // {path: '**', redirectTo: '/pagenotfound', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

