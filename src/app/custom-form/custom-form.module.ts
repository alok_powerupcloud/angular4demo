import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule} from '@angular/router';
import { FormComponent} from './form.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
  path: 'forms',
  component: FormComponent,
  } 
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FormComponent],
  exports: [
    RouterModule
  ]
})
export class CustomFormModule { }
