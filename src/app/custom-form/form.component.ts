import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  capitalApplicantform: FormGroup;

  constructor() { }

  ngOnInit() {
    this.capitalApplicantform = new FormGroup({
      name: new FormGroup({
        firstName: new FormControl(null, Validators.required),
        lastName: new FormControl(null, Validators.required)
      }),
      email: new FormControl(null, [Validators.required, Validators.email]),
      phone: new FormControl(null,Validators.required),
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      cnfrmPassword: new FormControl(null, Validators.required)
    });
  }

  /**
   * Method to add applicant
   */
  addCapitalApplicant(){
    console.log(this.capitalApplicantform);
  }
}
