import { Component } from '@angular/core';
import { AuthService } from './login/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private authService: AuthService, private router: Router, private toastr: ToastrService){}

  /**
   * Method to check if user is logged in
   */
  get isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }

  /**
   * Method to logout the user
   */
  logout() {
    this.authService.logout();
    this.toastr.success('You have successfully logged out');
    this.router.navigate(['/login']);
  }
}
