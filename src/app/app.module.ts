import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';

import {AppRoutingModule} from './app.routing.module';
import { EmployeesModule } from './employees/employees.module';
import { CustomersModule } from './customers/customers.module';
import { ChartsModule } from './charts/charts.module';
import { CustomFormModule } from './custom-form/custom-form.module';
import { LoginModule } from './login/login.module';
import { HomeModule } from './home/home.module';

import { AuthService } from './login/auth.service';
import { CanActivateAuthGuard } from './guards/can-activate-auth.guard';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    EmployeesModule,
    CustomersModule,
    ChartsModule,
    CustomFormModule,
    LoginModule,
    HomeModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [
    AuthService,
    CanActivateAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
